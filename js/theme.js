// JavaScript Document

jQuery(function($){

	"use strict";

	$(document).ready(function(){

		$("header .genesis-nav-menu").addClass("responsive-menu");
		$("header .title-area").after('<div class="responsive-menu-icon"></div>');

		$("header .responsive-menu-icon").click(function(){
			$("header .genesis-nav-menu").slideToggle();
		});


		$(window).resize(function(){
			if(window.innerWidth > 768) {
				$("header .genesis-nav-menu, nav .sub-menu").removeAttr("style");
				$(".responsive-menu > .menu-item").removeClass("menu-open");
			}
		});


		$(".responsive-menu > .menu-item").click(function(e){

			if( e.target !== this ) {
				return;
			}

			$(this).find(".sub-menu:first").slideToggle(function() {
				$(this).parent().toggleClass("menu-open");
			});

		});

		// Prevent empty WordPress searches - Genesis
		$(".search-form").submit(function(e) { 
			var f = $(this).find("input[type=search]");
			f.val($.trim(f.val()));
			if( !f.val() ){
				f.focus();
				e.preventDefault();
			}
		});


	}); // doc ready
}); // jQuery