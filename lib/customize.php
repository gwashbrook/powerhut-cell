<?php

/**
 * Customizer additions.
 *
 * @package Powerhut Cell
 * @author  Powerhut Net
 * @link    https://powerhut.net/themes/
 * @license GPL2-0+
 */


add_action( 'customize_register', 'child_customize_register' );

/**
 * Register settings and controls with the Customizer.
 *
 * @param WP_Customize_Manager $wp_customize Customizer object.
 */
function child_customize_register( $wp_customize ) {
   //All our sections, settings, and controls will be added here
   

	$wp_customize->add_setting(
		'child_primary_color',
		array(
			'default'           => child_customizer_get_default_primary_color(),
			'sanitize_callback' => 'sanitize_hex_color',
		)
	);

	$wp_customize->add_control(
		new WP_Customize_Color_Control(
			$wp_customize,
			'child_primary_color',
			array(
				'description' => __( 'Change the primary colour.', 'powerhut-cell' ),
				'label'       => __( 'Primary Colour', 'powerhut-cell' ),
				'section'     => 'colors',
				'settings'    => 'child_primary_color',
			)
		)
	);


   
} // fn child_customize_register
