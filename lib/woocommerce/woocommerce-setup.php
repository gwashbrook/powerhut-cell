<?php
/**
 * Vendeur Pro.
 *
 * This file adds the required WooCommerce setup functions to the Vendeur Pro Theme.
 *
 * @package Vendeur
 * @author  StudioPress
 * @license GPL-2.0+
 * @link    https://powerhut.net/themes/vendeur/
 */

// Includes the customizer settings for the WooCommerce plugin.
include_once( get_stylesheet_directory() . '/lib/woocommerce/woocommerce-customize.php' );

// Includes the customizer CSS for the WooCommerce plugin.
include_once( get_stylesheet_directory() . '/lib/woocommerce/woocommerce-output.php' );

// Includes notice to install Genesis Connect for WooCommerce.
include_once( get_stylesheet_directory() . '/lib/woocommerce/woocommerce-notice.php' );

// Includes functions for the WooCommerce plugin.
include_once( get_stylesheet_directory() . '/lib/woocommerce/woocommerce-functions.php' );

// Add Genesis Scripts support to the product post type (Not included in Genesis Connect for WooCommerce plugin. Pull request made https://github.com/copyblogger/genesis-connect-woocommerce/pull/13 )
add_action ('init','vendeur_woocommerce_init');
function vendeur_woocommerce_init() {
	// Add post type support
	$supports = array (
		// 'genesis-cpt-archives-settings',
		'genesis-scripts',
		// 'genesis-entry-meta-after-content',
	);
	add_post_type_support( 'product', $supports);
}

// Add product gallery support
if ( class_exists( 'WooCommerce' ) ) {
	add_theme_support( 'wc-product-gallery-lightbox' );
	add_theme_support( 'wc-product-gallery-slider' );
	add_theme_support( 'wc-product-gallery-zoom' );
}

add_filter( 'woocommerce_style_smallscreen_breakpoint', 'vendeur_woocommerce_breakpoint', 10, 1 );
/**
 * Modify the WooCommerce breakpoints.
 *
 * @since 
 */
function vendeur_woocommerce_breakpoint( $breakpoint ) {

	$current = genesis_site_layout();
	$layouts = array(
		'content-sidebar',
		'sidebar-content',
	);

	if ( in_array( $current, $layouts ) ) {
		$breakpoint = '1200px';
	}
	else {
		$breakpoint = '800px';
	}
	
	return $breakpoint;

}


// Set the default number of products per page. Overidden by Dashboard > WooCommerce > Settings > Genesis Connect Addons > Products per page
add_filter( 'genesiswooc_default_products_per_page', 'vendeur_default_products_per_page' );
function vendeur_default_products_per_page() {
	return 12;
}


// Modify default WooCommerce column count for product thumbnails. If Sidebar, 3 columns - else 4 columns
add_filter( 'loop_shop_columns', 'vendeur_product_archive_columns' );
function vendeur_product_archive_columns() {
	$current = genesis_site_layout();
	$layouts = array(
		'content-sidebar',
		'sidebar-content',
	);

	if ( in_array( $current, $layouts ) ) {
		return 3; // 3
	} else {
		return 4;
	}

	return $n_columns;
}



// Modify the archive pagination next and previous arrows to the default Genesis style
add_filter( 'woocommerce_pagination_args', 	'vendeur_woocommerce_pagination' );
function vendeur_woocommerce_pagination( $args ) {

	$args['prev_text'] = sprintf( '&laquo; %s', __( 'Previous Page', 'vendeur-pro' ) );
	$args['next_text'] = sprintf( '%s &raquo;', __( 'Next Page', 'vendeur-pro' ) );

	return $args;

}




add_action( 'after_switch_theme', 'vendeur_woocommerce_image_dimensions_after_theme_setup', 1 );
/**
 * Define WooCommerce image sizes on theme activation.
 *
 * @since 1.1.0
 */
function vendeur_woocommerce_image_dimensions_after_theme_setup() {

	global $pagenow;

	if ( ! isset( $_GET['activated'] ) || $pagenow != 'themes.php' || ! class_exists( 'WooCommerce' ) ) {
		return;
	}

	vendeur_update_woocommerce_image_dimensions();

}

add_action( 'activated_plugin', 'vendeur_woocommerce_image_dimensions_after_woo_activation', 10, 2 );
/**
 * Define the WooCommerce image sizes on WooCommerce activation.
 *
 * @since 1.1.0
 */
function vendeur_woocommerce_image_dimensions_after_woo_activation( $plugin ) {

	// Check to see if WooCommerce is being activated.
	if ( $plugin !== 'woocommerce/woocommerce.php' ) {
		return;
	}

	vendeur_update_woocommerce_image_dimensions();

}

/**
 * Update WooCommerce image dimensions.
 * https://docs.woocommerce.com/document/set-woocommerce-image-dimensions-upon-theme-activation/
 * @since 
 */
function vendeur_update_woocommerce_image_dimensions() {

	$catalog = array(
		'width'  => '530', // px
		'height' => '530', // px
		'crop'   => 1,     // true
	);
	$single = array(
		'width'  => '720', // px
		'height' => '720', // px
		'crop'   => 1,     // true
	);
	$thumbnail = array(
		'width'  => '180', // px
		'height' => '180', // px
		'crop'   => 1,     // true
	);

	// Image sizes.
	update_option( 'shop_catalog_image_size', $catalog );     // Product category thumbs.
	update_option( 'shop_single_image_size', $single );       // Single product image.
	update_option( 'shop_thumbnail_image_size', $thumbnail ); // Image gallery thumbs.

}


// add_filter( 'woocommerce_output_related_products_args', 'vendeur_related_products_args' );
/**
 * Changes number of related products on product page.
 *
 * @since 1.0.0
 */
function vendeur_related_products_args( $args ) {

	$args['posts_per_page'] = 3; // 3 related products.
	$args['columns'] = 3; // Arranged in 3 columns.
	return $args;

}




// Remove WooCommerce styles one by one
// add_filter( 'woocommerce_enqueue_styles', 'jk_dequeue_styles' );
function jk_dequeue_styles( $enqueue_styles ) {
	unset( $enqueue_styles['woocommerce-general'] );	// Remove the gloss
	unset( $enqueue_styles['woocommerce-layout'] );		// Remove the layout
	unset( $enqueue_styles['woocommerce-smallscreen'] );	// Remove the smallscreen optimisation
	return $enqueue_styles;
}

// Or just remove them all in one line
// add_filter( 'woocommerce_enqueue_styles', '__return_false' );