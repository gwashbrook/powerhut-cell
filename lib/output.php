<?php

add_action( 'wp_enqueue_scripts', 'child_css' );

function child_css() {

	$handle  = defined( 'CHILD_THEME_NAME' ) && CHILD_THEME_NAME ? sanitize_title_with_dashes( CHILD_THEME_NAME ) : 'child-theme';

	$color_primary = get_theme_mod( 'child_primary_color', child_customizer_get_default_primary_color() );
	// $color_accent = get_theme_mod( 'genesis_sample_accent_color', genesis_sample_customizer_get_default_accent_color() );

	$css = '';

	$css .= ( child_customizer_get_default_primary_color() !== $color_primary ) ? sprintf( '

		a,
		.entry-title a:focus,
		.entry-title a:hover,
		.genesis-nav-menu a:focus,
		.genesis-nav-menu a:hover,
		.genesis-nav-menu .current-menu-item > a,
		.genesis-nav-menu .sub-menu .current-menu-item > a:focus,
		.genesis-nav-menu .sub-menu .current-menu-item > a:hover,
		.menu-toggle:focus,
		.menu-toggle:hover,
		.sub-menu-toggle:focus,
		.sub-menu-toggle:hover {
			color: %1$s;
		}
		.site-header {
			background-color: %1$s;
		}

		', $color_primary ) : '';
	

	if ( $css ) {
		wp_add_inline_style( $handle, $css );
	}

}