<?php
/**
 * Powerhut Cell.
 *
 * This file adds functions to the Powerhut Cell theme.
 *
 * @package Powerhut Cell
 * @author  Graham Washbrook
 * @license GPL-2.0+
 * @link    https://powerhut.net/themes/
 */

//* Child theme (do not remove)
define( 'CHILD_THEME_NAME', 'Powerhut Cell' );
define( 'CHILD_THEME_URL', 'https://powerhut.net/themes/' );
define( 'CHILD_THEME_VERSION', '0.0.32' );

//* Start the engine
include_once( get_template_directory() . '/lib/init.php' );

//* Setup Theme
include_once( get_stylesheet_directory() . '/lib/theme-defaults.php' );

//* Set Localisation (do not remove)
add_action( 'after_setup_theme', 'child_localization_setup' );
function child_localization_setup(){
	load_child_theme_textdomain( 'powerhut-cell', get_stylesheet_directory() . '/languages' );
}

// Add the theme helper functions.
include_once( get_stylesheet_directory() . '/lib/helper-functions.php' );

//* Additions to WordPress Theme Customizer.
require_once( get_stylesheet_directory() . '/lib/customize.php' );

// Include Customizer CSS.
include_once( get_stylesheet_directory() . '/lib/output.php' );

//* Add WooCommerce support.
include_once( get_stylesheet_directory() . '/lib/woocommerce/woocommerce-setup.php' );

//* Enqueue Scripts and Styles
add_action( 'wp_enqueue_scripts', 'child_enqueue_scripts_styles' );
function child_enqueue_scripts_styles() {

    // Theme JS
    // wp_enqueue_script( 'theme-js', get_bloginfo( 'stylesheet_directory' ) . '/js/theme.js', array( 'jquery' ), CHILD_THEME_VERSION );
   
    // Google fonts
    // wp_enqueue_style( 'google-fonts', '//fonts.googleapis.com/css?family=Advent+Pro:300', array(), CHILD_THEME_VERSION );

    // Font Awesome
    wp_enqueue_style( 'font-awesome', '//maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css', array(), CHILD_THEME_VERSION );

	// Ionicons
	// wp_enqueue_style( 'ionicons', '//code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css', array(), CHILD_THEME_VERSION );

	// Dashicons
    wp_enqueue_style( 'dashicons' );
	
	$suffix = ( defined( 'SCRIPT_DEBUG' ) && SCRIPT_DEBUG ) ? '' : '.min';
	wp_enqueue_script( 'child-responsive-menu', get_stylesheet_directory_uri() . "/js/responsive-menus{$suffix}.js", array( 'jquery' ), CHILD_THEME_VERSION, true );
	wp_localize_script(
		'child-responsive-menu',
		'genesis_responsive_menu',
		child_responsive_menu_settings()
	);
	
}


//* Define our responsive menu settings.
function child_responsive_menu_settings() {

	$settings = array(
		'mainMenu'          => __( 'Menu', 'powerhut-cell' ),
		'menuIconClass'     => 'dashicons-before dashicons-menu',
		'subMenu'           => __( 'Submenu', 'powerhut-cell' ),
		'subMenuIconsClass' => 'dashicons-before dashicons-arrow-down-alt2',
		'menuClasses'       => array(
			'combine' => array(
				'.nav-primary',
				'.nav-header',
			),
			'others'  => array(),
		),
	);

	return $settings;
}

//* Add Theme Supports
// --------------------------- //

//* Add HTML5 markup structure
add_theme_support( 'html5', array( 'comment-list', 'comment-form', 'search-form', 'gallery', 'caption' ) );

//* Add Accessibility support
add_theme_support( 'genesis-accessibility', array( '404-page', 'drop-down-menu', 'headings', 'rems', 'search-form', 'skip-links' ) );

//* Add viewport meta tag for mobile browsers
add_theme_support( 'genesis-responsive-viewport' );

//* Allow plugins and themes to manage the document title tag. Since WP 4.1
add_theme_support( 'title-tag' );

//* Add selective refresh support for widgets.  Since WP 4.5
add_theme_support( 'customize-selective-refresh-widgets' );

//* Add support for custom background
add_theme_support( 'custom-background' );

//* Add support for custom header
add_theme_support( 'custom-header', array(
	'width'           => 240,
	'height'          => 55,
	'header-selector' => '.site-title a',
	'header-text'     => false,
	// 'flex-height'     => true,
	// 'flex-width'     => true,
) );

//* Add support for Genesis footer widgets
add_theme_support( 'genesis-footer-widgets', 3 );

//* Add support for after entry widget.
add_theme_support( 'genesis-after-entry-widget-area' );

//* Add support for structural wraps
add_theme_support( 'genesis-structural-wraps', array(
    'header',
    'menu-primary',
    // 'menu-secondary',
    'site-inner',
    'footer-widgets',
    'footer',
) );


//* Add support for additional style options
add_theme_support( 'genesis-style-selector', array(
	'child-avocado' => __( 'Avocado', 'powerhut-cell' ),
	'child-banana'  => __( 'Banana', 'powerhut-cell' ),
) );

//* Add support for custom nav menus
add_theme_support( 'genesis-menus', array(
	'primary'    => __( 'Header Menu', 'powerhut-cell' ),
	'secondary'  => __( 'Footer Menu', 'powerhut-cell' ),
	// 'off-canvas' => __( 'Off-Canvas Navigation Menu', 'powerhut-cell' ),
));


//* Layouts
// --------------------------- //

//* Remove the header right widget area
// unregister_sidebar( 'header-right' );

//* Remove secondary sidebar and associated layouts
unregister_sidebar( 'sidebar-alt' );
genesis_unregister_layout( 'content-sidebar-sidebar' );
genesis_unregister_layout( 'sidebar-sidebar-content' );
genesis_unregister_layout( 'sidebar-content-sidebar' );

//* Register custom layouts
add_action( 'init', 'child_customise_layouts' );
function child_customise_layouts() {
	 genesis_register_layout( 'content-no-sidebars', array(
		'label' => __('No Sidebars', 'powerhut-cell'),
		'img' => get_bloginfo('stylesheet_directory') . '/images/layout-content-no-sidebars.gif'
	) );
}

//* Remove sidebars on custom content-no-sidebars layout
add_action( 'genesis_before_content_sidebar_wrap', 'child_remove_sidebars' );
function child_remove_sidebars() {
	$site_layout = genesis_site_layout();
	if ( 'content-no-sidebars' === $site_layout ) {
		remove_action( 'genesis_after_content', 'genesis_get_sidebar' );
		remove_action( 'genesis_after_content_sidebar_wrap', 'genesis_get_sidebar_alt' );
	}
}

//* Add adjacent entry navigation
add_post_type_support( 'post', 'genesis-adjacent-entry-nav' );

//* Remove output of primary navigation right extras.
remove_filter( 'genesis_nav_items', 'genesis_nav_right', 10, 2 );
remove_filter( 'wp_nav_menu_items', 'genesis_nav_right', 10, 2 );

// Remove navigation meta box. // Remind me .. what is this?
add_action( 'genesis_theme_settings_metaboxes', 'child_remove_genesis_metaboxes' );
function child_remove_genesis_metaboxes( $_genesis_theme_settings_pagehook ) {
	remove_meta_box( 'genesis-theme-settings-nav', $_genesis_theme_settings_pagehook, 'main' );
}

//* Add new image sizes
add_image_size( 'single-cover', 848, 477, array( 'left', 'top' ) ); // 16:9


/* Reposition primary navigation menu to inside the header
remove_action( 'genesis_after_header', 'genesis_do_nav' );
// add_action( 'genesis_header', 'genesis_do_nav', 12 );
add_action( 'genesis_header', 'genesis_do_nav' );
*/


//* Reposition secondary menu to site footer
remove_action( 'genesis_after_header', 'genesis_do_subnav' );
add_action( 'genesis_footer', 'genesis_do_subnav', 7 );


//* Filter secondary navigation menu args
add_filter( 'wp_nav_menu_args', 'child_secondary_menu_args' );
function child_secondary_menu_args( $args ){
	if( 'secondary' != $args['theme_location'] ) return $args;
	$args['depth'] = 1;
	return $args;
}

//* Set search to posts only (Site Plugin territory)
add_filter( 'pre_get_posts', 'child_search_filter' );
function child_search_filter( $query ) {
	if ( ( $query->is_search ) && !is_admin() ) {
		$query->set( 'post_type', array('post') );
	}
	return $query;
}

//* Customise post archive entry
add_action('genesis_meta','child_customise_post_archive_entry');
function child_customise_post_archive_entry () {

	if( is_home() || is_tag() || is_category() || is_author() || is_date() || is_search() ){
		
		// Relocate featured image
		remove_action( 'genesis_entry_content', 'genesis_do_post_image', 8 );
		add_action( 'genesis_entry_header', 'genesis_do_post_image', 3 );

		// Remove post meta from entry-footer
		// remove_action( 'genesis_entry_footer', 'genesis_post_meta' );
				
		// Remove entry-footer markup
		// remove_action( 'genesis_entry_footer', 'genesis_entry_footer_markup_open', 5 );
		// remove_action( 'genesis_entry_footer', 'genesis_entry_footer_markup_close', 15 );
		

		
		
		// Wrap entry-header and entry-content
		add_action( 'genesis_entry_header', function(){ echo '<div class="entry-wrap">'; }, 4 ); // Might clash with post format image
		add_action( 'genesis_after_entry_content', function(){ echo '</div>'; }, 20 );
		// add_action( 'genesis_entry_footer', function(){ echo '</div>'; }, 9999 );

		// Remove the excerpt all together?
		// remove_action( 'genesis_entry_content', 'genesis_do_post_content' );
		// remove_action( 'genesis_entry_content', 'genesis_do_post_content_nav', 12 );
		// remove_action( 'genesis_entry_content', 'genesis_do_post_permalink', 14 );

		// Relocate entry-footer
		// remove_action( 'genesis_entry_footer', 'genesis_post_meta' );
		// add_action( 'genesis_entry_footer', 'genesis_post_meta', 21 );



	}
} // fn

//*
// add_filter ('shortcode_atts_post_categories', 'phut_shortcode_atts_post_categories' );
function phut_shortcode_atts_post_categories( $atts ) {
	// $atts['before'] = '<i class="fa fa-folder-o" aria-hidden="true"></i>&nbsp;&nbsp;';
	// $atts['before'] = '<span class="dashicons dashicons-category" aria-hidden="true"></span>';
	// $atts['before'] = '';
	return $atts;
}


//*
add_filter ('shortcode_atts_post_tags', 'phut_shortcode_atts_post_tags' );
function phut_shortcode_atts_post_tags( $atts ) {
	// $atts['before'] = '<i class="fa fa-tags" aria-hidden="true"></i>&nbsp;&nbsp;';
	// $atts['before'] = '<span class="dashicons dashicons-tag" aria-hidden="true"></span>';
	// $atts['before'] = '';
	$atts['sep'] = ' ';
	return $atts;
}


//* Filter the wp_tag_cloud args
add_filter('widget_tag_cloud_args','child_tag_cloud_args');
function child_tag_cloud_args( $args ) {
	$args['smallest'] = 1;
	$args['largest']  = 1;
	$args['unit']     = 'em';
	$args['order']    = 'DESC';
	$args['orderby']  = 'count';
	return $args;
}

//* Filter the post info
// $filtered = apply_filters( 'genesis_post_info', '[post_date] ' . __( 'by', 'powerhut-cell' ) . ' [post_author_posts_link] [post_comments] [post_edit]' );

add_filter('genesis_post_info','child_post_info');
function child_post_info( $filtered ) {

	$filtered = '[post_date] [post_comments] [post_edit]';
	return $filtered;
}

//* Replace Genesis search form
add_filter( 'genesis_search_form', 'child_search_form', 10, 4);
function child_search_form( $form, $search_text, $button_text, $label ) {
	if ( genesis_html5() && genesis_a11y( 'search-form' ) ) {
		$search_text = get_search_query() ? apply_filters( 'the_search_query', get_search_query() ) : apply_filters( 'genesis_search_text', __( 'Search this website', 'genesis' ) . ' &#x02026;' );
		$button_text = apply_filters( 'genesis_search_button_text', esc_attr__( 'Search', 'genesis' ) );
		$onfocus = "if ('" . esc_js( $search_text ) . "' === this.value) {this.value = '';}";
		$onblur  = "if ('' === this.value) {this.value = '" . esc_js( $search_text ) . "';}";
		// Empty label, by default. Filterable.
		$label = apply_filters( 'genesis_search_form_label', '' );
		$value_or_placeholder = ( get_search_query() == '' ) ? 'placeholder' : 'value';
		$form  = sprintf( '<form %s>', genesis_attr( 'search-form' ) );
		if ( '' == $label )  {
			$label = apply_filters( 'genesis_search_text', __( 'Search this website', 'genesis' ) );
		}
		$form_id = uniqid( 'searchform-' );
		$form .= sprintf(
			'<meta itemprop="target" content="%s"/><label class="search-form-label screen-reader-text" for="%s">%s</label><input itemprop="query-input" type="search" name="s" id="%s" %s="%s" /><button type="submit"><span class="screen-reader-text">%s</span></button></form>',
			home_url( '/?s={s}' ),
			esc_attr( $form_id ),
			esc_html( $label ),
			esc_attr( $form_id ),
			$value_or_placeholder,
			esc_attr( $search_text ),
			esc_attr( $button_text )
		);
	}
	return $form;
}

// REGISTER FRONT PAGE WIDGETS

//* Register widget areas
genesis_register_sidebar( array(
	'id'          => 'home-slider',
	'name'        => __( 'Home - Slider', 'powerhut-cell' ),
	'description' => __( 'This is the slider section of the homepage.', 'powerhut-cell' ),
) );
genesis_register_sidebar( array(
	'id'          => 'home-top',
	'name'        => __( 'Home - Top', 'powerhut-cell' ),
	'description' => __( 'This is the top section of the homepage.', 'powerhut-cell' ),
) );
genesis_register_sidebar( array(
	'id'          => 'home-bottom-left',
	'name'        => __( 'Home - Bottom Left', 'powerhut-cell' ),
	'description' => __( 'This is the bottom left section of the homepage.', 'powerhut-cell' ),
) );
genesis_register_sidebar( array(
	'id'          => 'home-bottom-right',
	'name'        => __( 'Home - Bottom Right', 'powerhut-cell' ),
	'description' => __( 'This is the bottom right section of the homepage.', 'powerhut-cell' ),
) );


//*
add_action ( 'genesis_before_entry', 'child_maybe_cover_featured_img' );
function child_maybe_cover_featured_img () {

	// Only for defined post types
	$post_types= array (
		'post',
		// 'phut_theme',
	);
	
	if( !is_singular( $post_types ) ) return;
	
	if ( has_post_thumbnail() ) {
		$cover_id = get_post_thumbnail_id( );
		$html = wp_get_attachment_image( $cover_id, 'single-cover', false, '' );
		echo '<div class="cover-wrap">' . $html . '</div>';
	}
}


//* Modify the size of the Gravatar in the author box
add_filter( 'genesis_author_box_gravatar_size', 'child_author_box_gravatar' );
function child_author_box_gravatar( $size ) {
	return 90;
}

//* Modify the size of the Gravatar in the entry comments
add_filter( 'genesis_comment_list_args', 'child_comments_gravatar' );
function child_comments_gravatar( $args ) {
	$args['avatar_size'] = 60;
	return $args;
}

//* Modify comment form defaults
add_filter( 'comment_form_defaults', 'child_modify_comment_form_defaults' );
function child_modify_comment_form_defaults( $defaults ) {
	// $defaults['title_reply'] = __( 'Add Your Story', 'powerhut-cell' );
	// $defaults['label_submit'] = __( 'Submit Story', 'powerhut-cell' );
	$defaults['comment_notes_after'] = '';
	return $defaults;
}

