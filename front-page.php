<?php
/**
 * This file adds the Home Page to the Powerhut Cell Theme.
 */








add_action( 'genesis_meta', 'child_home_genesis_meta' );
/**
 * Add widget support for homepage. If no widgets active, display the default loop.
 *
 */
function child_home_genesis_meta() {

	if ( is_active_sidebar( 'home-slider' ) || is_active_sidebar( 'home-top' ) || is_active_sidebar( 'home-middle' ) || is_active_sidebar( 'home-bottom-left' ) || is_active_sidebar( 'home-bottom-right' ) ) {

		// Force content-sidebar layout setting
		// add_filter( 'genesis_pre_get_option_site_layout', '__genesis_return_content_sidebar' );
		
		//* Forces full-width-content layout
		add_filter( 'genesis_pre_get_option_site_layout', '__genesis_return_full_width_content' );
		

		// Add child-home body class
		add_filter( 'body_class', 'child_body_class' );

		// Remove the default Genesis loop
		remove_action( 'genesis_loop', 'genesis_do_loop' );

		// Add homepage widgets
		add_action( 'genesis_loop', 'child_homepage_widgets' );

	}
}

function child_body_class( $classes ) {

	$classes[] = 'cell-home';
	return $classes;
	
}

function child_homepage_widgets() {


	if( is_active_sidebar( 'home-slider' ) ) {
		genesis_widget_area( 'home-slider', array(
			'before' => '<div class="home-slider widget-area">',
			'after'  => '</div>',
		) );
	}

	if( is_active_sidebar( 'home-top' ) ) {
		genesis_widget_area( 'home-top', array(
			'before' => '<div class="home-top widget-area">',
			'after'  => '</div>',
		) );
	}

	
	if ( is_active_sidebar( 'home-bottom-left' ) || is_active_sidebar( 'home-bottom-right' ) ) {

		echo '<div class="home-bottom">';

		genesis_widget_area( 'home-bottom-left', array(
			'before' => '<div class="one-half first widget-area home-bottom-left">',
			'after'  => '</div>',
		) );

		genesis_widget_area( 'home-bottom-right', array(
			'before' => '<div class="one-half widget-area home-bottom-right">',
			'after'  => '</div>',
		) );

		echo '</div>';
	
	}

}

genesis();
